import logging
from io import BytesIO

from PIL import Image
from django.core.files.base import ContentFile
from django.db.models.signals import pre_save
from django.dispatch import receiver

from main.models import VenuePhotos, VenueAward

THUMBNAIL_SIZE = (350, 175)

logger = logging.getLogger(__name__)


@receiver(pre_save, sender=VenuePhotos)
def generate_venue_thumbnail(sender, instance, **kwargs):
    logger.info(
        "Generating thumbnail for %s",
        instance.venue_photo.id,
    )
    image = Image.open(instance.image)
    image = image.convert("RGB")
    image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
    temp_thumb = BytesIO()
    image.save(temp_thumb, "JPEG")
    temp_thumb.seek(0)

    # set save=False, otherwise it will run in an infinite loop
    instance.venue_thumbnail.save(instance.image.name, ContentFile(temp_thumb.read()), save=False, )
    temp_thumb.close()


@receiver(pre_save, sender=VenueAward)
def generate_award_thumbnail(sender, instance, **kwargs):
    logger.info(
        "Generating thumbnail for %s", instance.award_photo.url,
    )
    image = Image.open(instance.award_photo)
    image = image.convert("RGB")
    image.thumbnail(THUMBNAIL_SIZE, Image.ANTIALIAS)
    temp_thumb = BytesIO()
    image.save(temp_thumb, "JPEG")
    temp_thumb.seek(0)

    # set save=False, otherwise it will run in an infinite loop
    instance.awards_thumbnail.save(instance.award_photo.name, ContentFile(temp_thumb.read()), save=False, )
    temp_thumb.close()
