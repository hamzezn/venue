# Create your models here.
from random import randint

from django.contrib.auth import get_user_model
from django.contrib.gis.db.models import PointField, Count
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from Venues import settings
from dj_array_field_persian.models.fields import ArrayField

User = get_user_model()


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(active=True)


class RandomManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index:random_index + 3]


class BaseModel(models.Model):
    """
    a abstract model - it's preferred for all models to inheritance from this model
    """
    slug = models.SlugField(max_length=100, null=False, unique=True, verbose_name=_('Slug'),
                            help_text="در صورت بروز خطا تغییر دهید")
    active = models.BooleanField(default=True, verbose_name="وضعیت فعال")
    updated_time = models.DateTimeField(verbose_name=_('Modified On'), auto_now=True)
    created_time = models.DateTimeField(verbose_name=_('Creation On'), auto_now_add=True, db_index=True)

    objects = models.Manager()
    active_objects = ActiveManager()
    random_objects = RandomManager()

    class Meta:
        abstract = True

    # @property
    # def jalali_updated_time(self):
    #     if self.updated_time:
    #         return JalaliDatetime(timezone.localtime(self.updated_time)).strftime('%H:%M %y/%m/%d')
    #     return None
    #
    # @property
    # def jalali_created_time(self):
    #     if self.created_time:
    #         return JalaliDatetime(timezone.localtime(self.created_time)).strftime('%H:%M %y/%m/%d')
    #     return None
    #
    # jalali_created_time.fget.short_description = _("Creation On")
    # jalali_updated_time.fget.short_description = _("Modified On")


class VenueAward(BaseModel):
    name = models.CharField(max_length=200, verbose_name="افتخارات")
    award_photo = models.ImageField(upload_to="awards-images", verbose_name="تصویر افتخار")
    awards_thumbnail = models.ImageField(upload_to="awards-thumbnails", null=True)

    class Meta:
        managed = True
        verbose_name = "افتخار"
        verbose_name_plural = "افتخارات"

    def __str__(self):
        return self.name


class VenueDetails(models.Model):
    container = models.OneToOneField('Venue', models.CASCADE, db_column='id', primary_key=True, db_index=True,
                                     verbose_name="تالار")
    notes = models.TextField(blank=True, verbose_name="یادداشت ها")
    amenities = ArrayField(models.CharField(max_length=80), size=10, default=list, blank=True, null=True,
                           verbose_name="قابلیت ها")
    restrictions = ArrayField(models.CharField(max_length=80), size=10, default=list, blank=True, null=True,
                              verbose_name="محدودیت ها")
    links = ArrayField(models.CharField(max_length=80), size=10, default=list, blank=True, null=True,
                       verbose_name="سایر لینک ها و شبکه های اجتماعی")

    # availability = ArrayField(models.CharField(max_length=20), size=10, default=list, blank=True,
    #                           verbose_name="روز های در دسترس")

    class Meta:
        managed = True
        db_table = 'venue_details'
        verbose_name = "جزییات"
        verbose_name_plural = "جزییات ها"

    def __str__(self):
        return self.container.name


class VenueEstimate(models.Model):
    container = models.OneToOneField('Venue', models.CASCADE, db_column='id', primary_key=True, db_index=True,
                                     verbose_name="تالار")
    base = models.BigIntegerField(blank=True, null=True, verbose_name="هزینه ورودی پایه به ازای هر مهمان",
                                  help_text="")
    Tax = models.FloatField(blank=True, null=True, verbose_name="مالیات بر ارزش افزوده (درصد)",
                            help_text="مقدار اضافه درصد را وارد کنید بدون هیچ علامت اضافه ایی مثلا : ۹ یا ۹.۵ ")

    class Meta:
        managed = True
        db_table = 'venue_estimate'
        verbose_name = "نرخ"
        verbose_name_plural = "سایر نرخ ها"

    def __str__(self):
        return self.container.name


class Menu(models.Model):
    container = models.ForeignKey("Venue", models.CASCADE, related_name='venue_menu', db_index=True, )
    key = models.CharField(max_length=80, verbose_name="غذا یا منو عذا")
    value = models.BigIntegerField(verbose_name="قیمت (تومان)")

    class Meta:
        verbose_name = "نرخ منو (غذا)"
        verbose_name_plural = "نرخ های غذا"

    def __str__(self):
        return self.container.name


class Beverage(models.Model):
    container = models.ForeignKey("Venue", models.CASCADE, related_name='venue_beverage', db_index=True)
    key = models.CharField(max_length=80, verbose_name="نوشیدنی")
    value = models.BigIntegerField(verbose_name="قیمت (تومان)")

    class Meta:
        verbose_name = "نرخ نوشیدنی"
        verbose_name_plural = "نرخ های نوشیدنی ها"

    def __str__(self):
        return self.container.name


class Day(models.Model):
    container = models.ForeignKey("Venue", models.CASCADE, related_name='venue_day', db_index=True)
    key = models.CharField(max_length=80, verbose_name="ایام هفته")
    value = models.FloatField(verbose_name="درصد",
                              help_text="مقدار اضافه درصد را وارد کنید بدون هیچ علامت اضافه ایی مثلا : ۹ یا ۹.۵ ")

    class Meta:
        verbose_name = "نرخ ایام هفته"
        verbose_name_plural = "نرخ های ایام"

    def __str__(self):
        return self.container.name


class Month(models.Model):
    container = models.ForeignKey("Venue", models.CASCADE, related_name='venue_month', db_index=True)
    key = models.CharField(max_length=80, verbose_name="ماه")
    value = models.FloatField(verbose_name="درصد",
                              help_text="مقدار اضافه درصد را وارد کنید بدون هیچ علامت اضافه ایی مثلا : ۹ یا ۹.۵ ")

    class Meta:
        verbose_name = "نرخ ماه"
        verbose_name_plural = "نرخ های ماه ها"

    def __str__(self):
        return self.container.name


class OtherPrice(models.Model):
    container = models.ForeignKey("Venue", models.CASCADE, related_name='venue_other', db_index=True)
    key = models.CharField(max_length=80, verbose_name="گزینه")
    value = models.BigIntegerField(verbose_name="قیمت (تومان)")

    class Meta:
        verbose_name = "نرخ گزینه"
        verbose_name_plural = "نرخ های گزینه ها"

    def __str__(self):
        return self.container.name


class OtherCoPrice(models.Model):
    container = models.ForeignKey("Venue", models.CASCADE, related_name='venue_co_other', db_index=True)
    key = models.CharField(max_length=80, verbose_name="گزینه")
    value = models.FloatField(verbose_name="درصد",
                              help_text="مقدار اضافه درصد را وارد کنید بدون هیچ علامت اضافه ایی مثلا : ۹ یا ۹.۵ ")

    class Meta:
        verbose_name = "گزینه ضریب دار"
        verbose_name_plural = "گزینه های ضریب دار"

    def __str__(self):
        return self.container.name


class VenuePhotos(models.Model):
    venue_photo = models.ForeignKey('Venue', models.CASCADE, related_name='venue_photos', db_column='venue',
                                    verbose_name="نام مجموعه", db_index=True, )
    image = models.ImageField(upload_to="venue-images", verbose_name="تصاویر")
    photographer = models.CharField(max_length=200, blank=True, verbose_name="نام عکاس")
    venue_thumbnail = models.ImageField(upload_to="venue-thumbnails", blank=True, verbose_name="نمایش تصویر")
    active = models.BooleanField(default=True, verbose_name="وضعیت فعال")

    objects = models.Manager()
    active_objects = ActiveManager()

    class Meta:
        managed = True
        db_table = 'venue_photos'
        verbose_name = "عکس"
        verbose_name_plural = "عکس های مجموعه ها"

    def __str__(self):
        return u'%s %s' % (self.venue_photo.types.first(), self.venue_photo.name)


class VenueProperty(BaseModel):
    name = models.CharField(max_length=300, verbose_name="ویژگی ها")

    class Meta:
        managed = True
        db_table = 'venue_properties'
        verbose_name = "ویژگی"
        verbose_name_plural = "ویژگی ها"

    def __str__(self):
        return self.name


class VenueStyle(BaseModel):
    name = models.CharField(max_length=30, verbose_name="نوع و سبک تالار")

    class Meta:
        managed = True
        db_table = 'venue_styles'
        verbose_name = "سبک"
        verbose_name_plural = "سبک ها"

    def __str__(self):
        return self.name


class VenueService(BaseModel):
    name = models.CharField(max_length=30, verbose_name="نوع خدمات")

    class Meta:
        managed = True
        db_table = 'venue_services'
        verbose_name = "نوع خدمات"
        verbose_name_plural = "خدمات دهی ها"

    def __str__(self):
        return self.name


class VenueSpace(BaseModel):
    name = models.CharField(max_length=30, verbose_name="نوع فضا")

    class Meta:
        managed = True
        db_table = 'venue_spaces'
        verbose_name = "نوع فضا"
        verbose_name_plural = "فضاها"

    def __str__(self):
        return self.name


class VenueTypeManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class VenueType(BaseModel):
    name = models.CharField(max_length=30, verbose_name="نوع مجموعه")
    description = models.TextField(blank=True, verbose_name="توضیحات")

    objects = VenueTypeManager()

    class Meta:
        managed = True
        db_table = 'venue_type'
        verbose_name = "نوع مجموعه"
        verbose_name_plural = "انواع مجموعه"

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.slug,)


class VenueProvince(BaseModel):
    name = models.CharField(max_length=30, verbose_name="استان",
                            help_text="")

    class Meta:
        managed = True
        verbose_name = "استان"
        verbose_name_plural = "استان ها"

    def __str__(self):
        return self.name


class VenueArea(BaseModel):
    province = models.ForeignKey(VenueProvince, models.CASCADE, verbose_name='استان', related_name='area',
                                 db_index=True, )
    name = models.CharField(max_length=30, verbose_name="شهر و منطقه",
                            help_text="لطقا شهر و منطقه را پشت سر هم بنویسید و با علامت '/' تفکیک کنید ")

    class Meta:
        managed = True
        verbose_name = "شهر و منطقه"
        verbose_name_plural = "شهر و منطقه ها"

    def __str__(self):
        return self.name


class Venue(BaseModel):
    name = models.CharField(max_length=255, verbose_name="اسم مجموعه")
    phone = models.CharField(max_length=13, verbose_name="شماره تماس")
    area = models.ForeignKey(VenueArea, models.CASCADE, verbose_name="شهر / منطقه")
    address = models.CharField(max_length=500, blank=True, null=False, verbose_name="آدرس")
    coordinates = PointField(max_length=50, blank=True, verbose_name="مختصات جغرافیایی")
    capacity = models.PositiveSmallIntegerField(blank=False, null=False, verbose_name="ظرفیت")
    types = models.ManyToManyField(VenueType, blank=True, verbose_name="کاربری های مجموعه", )
    styles = models.ManyToManyField(VenueStyle, blank=True, verbose_name="سبک")
    services = models.ManyToManyField(VenueService, blank=True, verbose_name="نوع سرویس دهی ها")
    spaces = models.ManyToManyField(VenueSpace, blank=True, verbose_name="نوع فضا")
    awards = models.ManyToManyField(VenueAward, blank=True, verbose_name="افتخارات")
    properties = models.ManyToManyField(VenueProperty, blank=True, verbose_name="ویژگی ها")
    description = models.TextField(blank=True, null=True, verbose_name="توضیحات")
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, editable=True, verbose_name="مالک")
    min_price = models.PositiveIntegerField(blank=False, null=False, verbose_name="کف قیمت")
    max_price = models.PositiveIntegerField(blank=False, null=False, verbose_name="سقف قیمت")
    rating = models.FloatField(blank=True, null=True, verbose_name="امتیاز")
    website = models.URLField(blank=True, null=True, verbose_name='وب سایت')

    class Meta:
        managed = True
        db_table = 'venues'
        verbose_name = "مجموعه"
        verbose_name_plural = "مجموعه ها"
        ordering = ['name']

    def __str__(self):
        return self.name

    @property
    def get_absolute_url(self):
        return reverse("venue_details", kwargs={"slug": self.slug})

    def get_estimate_url(self):
        return reverse("venue_estimate", kwargs={"slug": self.slug})

    def get_like_url(self):
        return reverse("venue_like_toggle", kwargs={"slug": self.slug})

    def get_api_like_url(self):
        return reverse("venue_like_api_toggle", kwargs={"slug": self.slug})

    def get_data_json_url(self):
        return reverse("venue_map_json_data", kwargs={"slug": self.slug})

    @property
    def picture_url(self):
        try:
            pic_url = self.venue_photos.first().venue_thumbnail.url
            return pic_url
        except:
            return "/static/images/noimage.svg"

    @cached_property
    def related_items(self):
        qs = Venue.objects \
            .filter(active=True) \
            .filter(area=self.area) \
            .exclude(pk=self.pk)
        return qs[:settings.RELATED_LIMIT]


class VenueRequest(models.Model):
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, related_name='sender', db_column='sender',
                               verbose_name="فرستنده", null=True, blank=True)
    receiver = models.ForeignKey(Venue, models.CASCADE, related_name='receiver', db_column='receiver',
                                 verbose_name="گیرنده")
    phone_number = PhoneNumberField(blank=False, null=False, verbose_name='شماره تلفن')
    number_of_guests = models.IntegerField(blank=True, default=1, verbose_name='تعداد مهمانان')
    contact_reason = models.CharField(blank=False, null=False, max_length=150, verbose_name='دلیل تماس با مجموعه')
    # preferred_wedding_date = models.DateField(blank=True, null=True, verbose_name='روز ترجیحی برای برگزاری جشن')
    preferred_wedding_date = models.DateTimeField(blank=True, null=True, verbose_name='روز ترجیحی برای برگزاری جشن')
    flexible_dates = models.BooleanField(blank=True, default=True, verbose_name='تاریخ قابل انعطاف')
    message = models.TextField(max_length=1000, verbose_name="متن پیام")

    status = models.SmallIntegerField(blank=True, default=0, verbose_name="وضعیت")
    read = models.NullBooleanField(blank=True, default=False, verbose_name="خوانده شده")
    created_time = models.DateTimeField(auto_now_add=True, blank=True, verbose_name="تاریخ و زمان ارسال")

    class Meta:
        managed = True
        db_table = 'user_requests'
        verbose_name = "درخواست کاربر"
        verbose_name_plural = 'درخواست های کاربران'

    def __str__(self):
        return u'از %s به %s' % (self.phone_number, self.receiver.name)


class SubmitVenue(models.Model):
    phone_number = PhoneNumberField(unique=True, blank=False, null=False, verbose_name='شماره تلفن')
    user_name = models.CharField(max_length=255, blank=False, null=False, verbose_name='نام صاحب تالار')
    email = models.EmailField(max_length=255, blank=True, null=True, verbose_name='ایمیل')
    area = models.CharField(max_length=255, blank=False, null=False, verbose_name='شهر/منطقه')
    venue_name = models.CharField(max_length=255, blank=False, null=False, verbose_name='نام تالار')
    website = models.URLField(blank=True, null=True, verbose_name='وب سایت تالار')
    created_time = models.DateTimeField(verbose_name=_('Creation On'), auto_now_add=True, db_index=True)

    class Meta:
        managed = True
        db_table = 'submit_venues'
        verbose_name = "درخواست همکاری"
        verbose_name_plural = 'درخواست های همکاری'

    def __str__(self):
        return u'درخوست %s برای ثبت تالار %s' % (self.user_name, self.venue_name)


class OtherFields(models.Model):
    name = models.CharField(max_length=500, verbose_name="نام سایت")
    about_us = models.TextField(max_length=10000, verbose_name="درباره ما")
    how_work = models.TextField(max_length=10000, verbose_name="نحوه کار")
    terms = models.TextField(max_length=1000, verbose_name="قوانین و مقررات")
    address = models.CharField(max_length=300, verbose_name="آدرس")
    phone = models.CharField(max_length=30, verbose_name="تلفن تماس")
    mobile = models.CharField(max_length=30, verbose_name="شماره تلفن همراه")
    email = models.EmailField(verbose_name="ایمیل")
    instagram = models.URLField(verbose_name="لینک اینستاگرام")
    telegram = models.URLField(verbose_name="لینک تلگرام")
    whats_app = models.URLField(verbose_name="لینک واتس اپ")
    copyright = models.CharField(max_length=800, verbose_name="متن گپی رابت")

    class Meta:
        managed = True
        verbose_name = "اطلاعات مربوط به سایت"
        verbose_name_plural = 'اطلاعات مربوط به سایت'

    def __str__(self):
        return 'ویرایش اطلاعات سایت'

    def save(self):
        # count will have all of the objects from the Aboutus model
        count = OtherFields.objects.all().count()
        # this will check if the variable exist so we can update the existing ones
        save_permission = OtherFields.has_add_permission(self)

        # if there's more than two objects it will not save them in the database
        if count < 2:
            super(OtherFields, self).save()
        elif save_permission:
            super(OtherFields, self).save()

    def has_add_permission(self):
        return OtherFields.objects.filter(id=self.id).exists()
