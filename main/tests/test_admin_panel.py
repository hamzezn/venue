from django.test import TestCase, Client

from user.models import User


class TestAdminPanel(TestCase):
    def create_user(self):
        self.phone_number = "+989123456789"
        self.password = User.objects.make_random_password()
        user = User.objects.create(phone_number=self.phone_number)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

    def test_spider_admin(self):
        self.create_user()
        client = Client()
        client.login(phone_number=self.phone_number, password=self.password)
        admin_pages = [
            # put all the admin pages for your models in here.
            "/admin/main/venueaward/",
            # "/admin/main/venuedetails/",
            "/admin/main/venueservice/",
            "/admin/main/venuestyle/",
            "/admin/main/venuearea/",
            "/admin/main/venuephotos/",
            "/admin/main/venuespace/",
            "/admin/main/venue/",
            "/admin/main/venueestimate/",
            "/admin/main/venueproperty/",
        ]
        for page in admin_pages:
            response = client.get(page)
            assert response.status_code == 200
            self.assertContains(response, "<!DOCTYPE html>")
