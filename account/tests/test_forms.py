from django.test import TestCase, Client

from account import forms
from django.contrib.auth import authenticate, get_user_model

class AccountFormTest(TestCase):
    def setUp(self):
        """Creat user at the first"""
        self.client = Client()
        self.phone_number = '+989123456789'
        self.password = '1234 qwer'
        self.user = get_user_model().objects.create(phone_number=self.phone_number, password=self.password,
                                                    is_active=True, is_staff=True,
                                                    is_superuser=True)
        self.user.set_password(self.password)
        self.user.save()
        self.user = authenticate(phone_number=self.phone_number, password=self.password)

    # def test_change_account_form_valid(self):
    #     """ Test that change account form is valid with valid values """
    #     forms_data = {
    #         'email': 'example@example.com',
    #         'name': 'Test Name',
    #     }
    #     form = forms.ChangeAccountForm(self.user, data=forms_data)
    #     self.assertFalse(form.is_valid())


    # def test_change_password_form_valid(self):
    #     """ Test that change password form will be valid with valid values """
    #     self.client.login(phone_number=self.phone_number, password=self.password)
    #     forms_data = {
    #         'old_password': self.password,
    #         'new_password1': 'Test pass',
    #         'new_password2': 'Test pass',
    #     }
    #     form = forms.ChangePasswordForm(self.user, data=forms_data)
    #     self.assertTrue(form.is_valid())
