from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

User = get_user_model()


def make_active(queryset):
    queryset.update(is_active=True)


make_active.short_description = "فعال سازی انتخاب شده ها"


def make_inactive(queryset):
    queryset.update(is_active=False)


make_inactive.short_description = "غیر فعال سازی انتخاب شده ها"


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    fieldsets = (
        (None, {"fields": ("phone_number", "password", "favorite_venues")}),
        (
            "اطلاعات شخصی",
            {"fields": ("name", 'email')},
        ),
        (
            "دسترسی ها",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (
            "زمان های مهم",
            {"fields": ("last_login", "date_joined")},
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("phone_number", "password1", "password2"),
            },
        ),
    )
    list_display = (
        "phone_number",
        "name",
        "is_active",
        "is_staff",
        "is_superuser",)
    list_editable = (
        "name",
        "is_active",
        "is_staff",
        "is_superuser",
    )
    search_fields = ("phone_number", "name",)
    ordering = ("phone_number",)
    actions = [make_active, make_inactive]
    readonly_fields = ("date_joined", "last_login")
    exclude = ('last_name', 'first_name', 'username')
