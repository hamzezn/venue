from datetime import datetime, timedelta

from bootstrap_modal_forms.mixins import PopRequestMixin, LoginAjaxMixin
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.forms import ValidationError
from django.utils import timezone
from django.utils.html import format_html
from django.utils.timezone import get_current_timezone
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.validators import validate_international_phonenumber

from user.models import User, Activation


class UserCacheMixin:
    user_cache = None


class SignUpForm(PopRequestMixin, UserCreationForm, LoginAjaxMixin):
    phone_number = PhoneNumberField(label='شماره تلفن', help_text='یک شماره تلفن معتبر وارد کنید.', required=True,
                                    error_messages={
                                        'invalid': 'لطفا یک شماره تلفن معتبر وارد کنید'
                                    })

    class Meta:
        model = get_user_model()
        fields = settings.SIGN_UP_FIELDS

    def clean(self):
        phone_number = self.cleaned_data.get('phone_number')
        validate_international_phonenumber(phone_number)
        user = User.objects.filter(phone_number=phone_number).exists()
        if user:
            user = User.objects.filter(phone_number=phone_number).first()
            if user.is_active:
                raise ValidationError('کاربری با این شماره تلفن موجود است.')
            elif not user.is_active:
                # raise ValidationError("شما قبلا ثبت نام شده اید جهت فعال سازی روی کلید فعال سازی کلیک کنید")
                raise ValidationError(format_html(
                    'شما قبلا ثبت نام شده اید جهت فعال سازی <a href="/user/resend/activation-code/">اینجا</a> کلیک کنید')
                )
        return self.cleaned_data


class LogInForm(AuthenticationForm):
    class Meta:
        model = get_user_model()

    def clean(self):
        phone_number = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        validate_international_phonenumber(phone_number)

        if phone_number is not None and password:
            self.user_cache = authenticate(self.request, username=phone_number, password=password)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data


class ChangeProfileForm(UserChangeForm):
    class Meta:
        model = get_user_model()
        fields = ('email', 'name',)


class ActivationForm(UserCacheMixin, forms.Form):
    code = forms.IntegerField(max_value=999999)

    def clean_code(self):
        code = self.cleaned_data['code']
        time_threshold = datetime.now(tz=get_current_timezone()) - timedelta(minutes=10)
        activation = Activation.objects.filter(code=code, created_at__gte=time_threshold).first()

        if not activation:
            raise ValidationError('کد فعالسازی اشتباه است یا قبلا مورد استفاده قرار گرفته است.')

        self.user_cache = activation.user
        return code


class ResendActivationCodeForm(UserCacheMixin, forms.Form):
    phone_number = PhoneNumberField(label='شماره تلفن', help_text='یک شماره تلفن معتبر وارد کنید.', required=True,
                                    error_messages={
                                        'invalid': 'لطفا یک شماره تلفن معتبر وارد کنید'
                                    })

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        user = User.objects.filter(phone_number=phone_number).first()
        if not user:
            raise ValidationError('شماره تلفن نامعتبر است.')

        if user.is_active:
            raise ValidationError('این حساب کاربری هم اکنون فعال است.')

        activation = Activation.objects.filter(user=user).first()
        if not activation:
            raise ValidationError('کد فعالسازی اشتباه است.')

        time_threshold = timezone.now() - timedelta(minutes=10)
        print(time_threshold)
        if activation.created_at > time_threshold:
            raise ValidationError('کد فعالسازی هم اکنون ارسال شده است آن را وارد کنید.')

        self.user_cache = user

        return phone_number


class RestorePasswordForm(UserCacheMixin, forms.Form):
    phone_number = PhoneNumberField(label='شماره تلفن', help_text='یک شماره تلفن معتبر وارد کنید.', required=True,
                                    error_messages={
                                        'invalid': 'لطفا یک شماره تلفن معتبر وارد کنید'
                                    })

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        user = User.objects.filter(phone_number=phone_number, is_active=True).first()

        if not user:
            raise ValidationError('کاربری با این شماره تلفن فعال نمی باشد یک شماره تلفن معتبر وارد کنید.')

        self.user_cache = user

        return phone_number


class RestorePasswordDoneForm(UserCacheMixin, forms.Form):
    code = forms.IntegerField(max_value=999999)

    def clean_code(self):
        code = self.cleaned_data['code']
        time_threshold = datetime.now() - timedelta(minutes=10)
        activation = Activation.objects.filter(code=code, created_at__gte=time_threshold).first()
        if not activation:
            raise ValidationError('کد فعالسازی اشتباه است یا منقضی شده است.')
        self.user_cache = activation.user

        return code


class RestorePasswordConfirmForm(forms.Form):
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
