from main import models


def venue_area_list(request):
    provinces = models.VenueProvince.objects.all()
    other = models.OtherFields.objects.first()
    return {
        'Provinces': provinces,
        'other': other,
    }
