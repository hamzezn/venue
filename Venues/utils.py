from kavenegar import *
import os
from dotenv import load_dotenv

load_dotenv()

try:
    import json
except ImportError:
    import simplejson as json


def send_sms(receptor, message):
    try:
        api = KavenegarAPI(os.getenv('SMS_API_KEY'))
        params = {
            'sender': '10004346',
            'receptor': str(receptor),
            'message': message,
        }
        response = api.sms_send(params)
        print(str(response))
    except APIException as e:
        print(str(e))
    except HTTPException as e:
        print(str(e))
    # print('+++++++++++++++++++++++++++++++++++++++++')
    # print(str(receptor))
    # print(str(message))
