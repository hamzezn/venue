from django.test import TestCase

from main import models
from main.factories import TestVenueFactory


class TestModel(TestCase):
    def test_active_manager_works(self):
        TestVenueFactory.create_batch(4, active=True)
        TestVenueFactory.create_batch(4, active=False)
        self.assertEqual(len(models.Venue.active_objects.all()), 4)
