from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class UserModelTest(TestCase):

    def test_create_user_phone_number_successful_not_active(self):
        """
        Test creating a new user with a phone_number is successful
        """
        phone_number = '+989123456789'
        password = 'testpass'

        user = User.objects.create_user(
            phone_number=phone_number,
            password=password,
        )
        self.assertEqual(user.phone_number, phone_number)
        self.assertTrue(user.check_password(password))
        self.assertFalse(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_user_no_phone_number(self):
        """
        Test create new user with no phone number rises error
        """
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')

    def test_create_user_invalid_phonenumber(self):
        """
        Test create new user with invalid phone number rises error
        """
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user('wrong phone', 'test123')

    def test_create_new_superuser(self):
        """
        Test creating a new superuser
        """
        admin_user = User.objects.create_superuser(
            phone_number='+989123456789',
            password='test123',
        )
        self.assertEqual(admin_user.phone_number, '+989123456789')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
