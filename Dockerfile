# Pull base image
#FROM python:3.7
FROM thinkwhere/gdal-python:3.7-ubuntu
# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
# Set work directory
WORKDIR /venue

# Install dependencies
COPY Pipfile Pipfile.lock /venue/
RUN pip3 install pipenv && pipenv install --system && apt update && apt install -y gettext libgettextpo-dev
# Copy project
COPY . /venue/
