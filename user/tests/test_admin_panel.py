from django.contrib.auth import get_user_model
from django.test import TestCase, Client

User = get_user_model()


class TestAdminPanel(TestCase):
    def create_user(self):
        self.phone_number = "+989123456789"
        self.password = User.objects.make_random_password()
        user = User.objects.create(phone_number=self.phone_number)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

    def test_spider_admin(self):
        self.create_user()
        client = Client()
        client.login(phone_number=self.phone_number, password=self.password)
        admin_pages = [
            # put all the admin pages for your models in here.
            "/admin/",
            "/admin/auth/",
            "/admin/auth/group/",
            "/admin/auth/group/add/",
            "/admin/user/user/",
            "/admin/user/user/add/",
            "/admin/password_change/",
        ]
        for page in admin_pages:
            response = client.get(page)
            assert response.status_code == 200
            self.assertContains(response, "<!DOCTYPE html>")

        admin_redirect_page = [
            "/admin/login/",
            "/admin/logout/",
        ]
        for page in admin_redirect_page:
            response = client.get(page)
            assert response.status_code == 302  # Redirect Code (302)
