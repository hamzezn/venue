from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'
    verbose_name = "برنامه"

    def ready(self):
        from main import signals
