from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404, render_to_response
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, RedirectView, TemplateView, FormView, CreateView
from django_filters.views import FilterView
from djgeojson.views import GeoJSONLayerView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.
from main import models
from main.filters import VenueFilter
from main.forms import SendRequestForm


def handler404(request, exception, template_name="404.html"):
    response = render_to_response("main/404.html")
    response.status_code = 404
    return response


class HomePageView(TemplateView):
    template_name = 'main/index.html'
    extra_context = {'HomeVenues': models.Venue.active_objects.all()[:6]}  # not complete


class AboutUsPageView(TemplateView):
    template_name = 'main/about_us.html'


class VenueFilterView(FilterView):
    filterset_class = VenueFilter
    template_name = 'main/venue_list.html'
    paginate_by = 10
    extra_context = {
        'Areas': models.VenueArea.active_objects.all(),
        'Styles': models.VenueStyle.active_objects.all(),
        'Services': models.VenueService.active_objects.all(),
    }

    def get_queryset(self):
        venues = models.Venue.active_objects.all()
        return venues.order_by("rating", "-updated_time")


class LocationListView(ListView):
    context_object_name = 'venues'
    paginate_by = 10
    template_name = 'main/venue_list.html'

    def get_queryset(self):
        style = self.kwargs['style']
        self.style = None
        if style != 'all':
            self.style = get_object_or_404(models.VenueStyle, slug=style)
        if self.style:
            venues = models.Venue.objects.active().filter(style=self.style)
        else:
            venues = models.Venue.objects.active()
        return venues.order_by("name")


class VenueDetails(DetailView, FormView):
    model = models.Venue
    template_name = "main/venue_details.html"
    context_object_name = 'venue'
    form_class = SendRequestForm
    success_message = 'درخواست شما با موفقیت ارسال شد.'
    success_url = reverse_lazy('venues_list')

    def get_initial(self):
        initial = super().get_initial()
        initial['message'] = "سلام، من شما رو توی وبسایت اجاره‌ای پیدا کردم و مایلم اطلاعات بیشتری در مورد مراسم " \
                             "در تالار شما داشته باشم. "
        return initial

    def form_valid(self, form):
        slug = self.kwargs.get('slug')
        user = self.request.user
        venue = get_object_or_404(models.Venue, slug=slug)
        # I think there is a better way to implement the following code
        phone_number = form.cleaned_data['phone_number']
        number_of_guests = form.cleaned_data['number_of_guests']
        contact_reason = form.cleaned_data['contact_reason']
        preferred_wedding_date = form.cleaned_data['preferred_wedding_date']
        flexible_dates = form.cleaned_data['flexible_dates']
        message = form.cleaned_data['message']
        if not user.is_authenticated:
            user = None
        models.VenueRequest.objects.create(sender=user, receiver=venue, phone_number=phone_number,
                                           number_of_guests=number_of_guests, contact_reason=contact_reason,
                                           preferred_wedding_date=preferred_wedding_date, flexible_dates=flexible_dates,
                                           message=message)
        messages.success(self.request, 'درخواست شما ارسال شد')
        return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        """If the form is invalid, render the invalid form."""
        messages.warning(self.request, 'مقدار ورودی نامعتبر است')
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class VenueEstimateView(VenueDetails, ):
    template_name = 'main/venue_estimate.html'


class VenueLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        slug = self.kwargs.get("slug")
        obj = get_object_or_404(models.Venue, slug=slug)
        url_ = obj.get_absolute_url
        user = self.request.user
        if user.is_authenticated:
            if obj in user.favorite_venues.all():
                user.favorite_venues.remove(obj)
            else:
                user.favorite_venues.add(obj)
        return url_


class VenueLikeAPIToggle(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, slug=None, format=None):
        # slug = self.kwargs.get("slug")
        obj = get_object_or_404(models.Venue, slug=slug)
        url_ = obj.get_absolute_url
        user = self.request.user
        updated = False
        liked = False
        if user.is_authenticated:
            if obj in user.favorite_venues.all():
                liked = False
                user.favorite_venues.remove(obj)
            else:
                liked = True
                user.favorite_venues.add(obj)
            updated = True
        data = {
            "updated": updated,
            "liked": liked
        }
        return Response(data)


class MapDataJsonView(GeoJSONLayerView):
    model = models.Venue
    # queryset = Venue.active_objects.filter(id=1)
    geometry_field = 'coordinates'
    properties = ('name', 'description', 'picture_url', 'get_absolute_url')
    with_modelname = False


class VenueMapDataJsonView(GeoJSONLayerView):
    geometry_field = 'coordinates'
    properties = ('name', 'picture_url', 'get_absolute_url')
    with_modelname = False

    def get_queryset(self):
        slug = self.kwargs.get("slug")
        return models.Venue.active_objects.filter(slug=slug)


class SubmitVenuePageView(SuccessMessageMixin, CreateView):
    template_name = 'main/submit_venue.html'
    model = models.SubmitVenue
    fields = ('phone_number', 'user_name', 'email', 'area', 'venue_name', 'website',)
    success_url = reverse_lazy('home')
    success_message = 'درخواست شما با موفقیت ثبت شد در اسرع وقت باشما تماس گرفته میشود'
