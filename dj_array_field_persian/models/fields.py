from django.contrib.postgres.fields import ArrayField as DjangoArrayField

from dj_array_field_persian.forms.fields import DynamicArrayField


class ArrayField(DjangoArrayField):
    def formfield(self, **kwargs):
        return super().formfield(**{
            "form_class": DynamicArrayField,
            'base_field': self.base_field.formfield(),
            'max_length': self.size,
            **kwargs})
