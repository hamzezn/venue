# Generated by Django 2.2.7 on 2020-01-14 14:58

import dj_array_field_persian.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_auto_20200114_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='venuedetails',
            name='links',
            field=dj_array_field_persian.models.fields.ArrayField(base_field=models.CharField(max_length=80), blank=True, default=list, null=True, size=10, verbose_name='سایر لینک ها و شبکه های اجتماعی'),
        ),
    ]
