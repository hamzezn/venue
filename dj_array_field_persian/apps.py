from django.apps import AppConfig


class DjArrayFieldPersianConfig(AppConfig):
    name = 'dj_array_field_persian'
