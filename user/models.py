from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class UserManager(BaseUserManager):

    def create_user(self, phone_number, password=None, **extra_fields):
        """Creates and saves a new user"""
        if not phone_number:
            raise ValueError('Users must have an phone_number')
        user = self.model(phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, phone_number, password):
        """Creates and saves a new super user"""
        user = self.create_user(phone_number, password)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that supports using phone_number instead of username"""
    phone_number = PhoneNumberField(unique=True, blank=False, null=False, verbose_name='شماره تلفن')
    name = models.CharField(max_length=255, blank=True, verbose_name='نام کامل')
    is_active = models.BooleanField(default=False, verbose_name='فعال')
    is_staff = models.BooleanField(default=False, verbose_name='دسترسی به پنل')
    date_joined = models.DateTimeField(verbose_name='زمان به ما پیوستن', auto_now_add=True)
    favorite_venues = models.ManyToManyField('main.Venue', blank=True, related_name='lovers',
                                             verbose_name="علاقه مندی ها")

    # Not use for auth just in profile can set it freely
    email = models.EmailField(max_length=255, blank=True, verbose_name='ایمیل')

    objects = UserManager()

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'کاربر'
        verbose_name_plural = 'کاربران'

    def __str__(self):
        # Add phone number to the end of the name in admin
        return self.name

    @property
    def is_venue_owner(self):
        return self.is_active and (
                self.is_superuser
                or self.is_staff
                and self.groups.filter(name="VenueOwner").exists()
        )


class Activation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.IntegerField(unique=True, verbose_name="کد")
