from django.test import TestCase
from django.test.client import Client
from django.contrib.auth import get_user_model, authenticate
from django.urls import reverse



class TestPages(TestCase):

    def setUp(self):
        """
        Create user for tests and initial client
        """

        self.phone_number = '+989123456789'
        self.password = '1234 qwer'

        self.client = Client()
        self.user = get_user_model().objects.create(phone_number=self.phone_number, password=self.password, is_active=True, is_staff=True,
                                        is_superuser=True)
        self.user.set_password(self.password)
        self.user.save()
        self.user = authenticate(phone_number=self.phone_number, password=self.password)

    def test_singup_page(self):
        """
        Test that sing up page works for not logged in users
        """
        response = self.client.get(reverse('sign_up_user'))
        self.assertEqual(response.status_code, 200)


    def test_login_page_only_guest(self):
        """
        Test that log in page works for only guests
        """
        self.client.login(phone_number=self.phone_number, password=self.password)
        response = self.client.get(reverse('log_in'))
        self.assertEqual(response.status_code, 302)

    def test_logout_redirect_guests(self):
        """
        test that logout page will redirect for guests
        """
        response = self.client.get(reverse('log_out'))
        self.assertEqual(response.status_code, 302)

    def test_logout_page_logged_in_users(self):
        """
        test that log out works for only logged in users
        """
        self.client.login(phone_number=self.phone_number, password=self.password)
        response = self.client.get(reverse('log_out'))
        self.assertEqual(response.status_code, 302)

    def test_restore_password(self):
        """
        test that reset password will work for guests
        """
        response = self.client.get(reverse('restore_password'))
        self.assertEqual(response.status_code, 200)

    def test_restore_password_done(self):
        """ test that restore done work for guests users """
        respones = self.client.get(reverse('restore_password_done'))
        self.assertEqual(respones.status_code, 200)

    def test_restore_confirm_not_works_for_guests(self):
        """ test that restore confirm will work for logged in users """
        respones = self.client.get(reverse('restore_password_confirm'))
        self.assertEqual(respones.status_code, 302)

