import random

import factory.fuzzy
from django.contrib.auth import get_user_model

User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    # phone_number = factory.Sequence(lambda n: '+9891234%d' % (n + 10000))
    name = factory.Faker("name", locale='fa_IR')
    password = "password"

    class Meta:
        model = User
        django_get_or_create = ('phone_number',)

    @factory.sequence
    def phone_number(n):
        a = random.randint(100000, 10000000)
        b = random.randint(1, 3)
        return '+989%d%08d' % (b, a)
