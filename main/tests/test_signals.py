from django.core.files.images import ImageFile
from django.test import TestCase

from main import models, factories


class TestSignal(TestCase):
    def test_thumbnails_are_generated_on_save(self):
        venue = factories.TestVenueFactory(name="The Iran", )

        with open(
                "main/fixtures/Venue1.png", "rb"
        ) as f:
            image = models.VenuePhotos(
                venue_photo=venue,
                image=ImageFile(f, name="tctb.jpg"),
            )
            with self.assertLogs("main", level="DEBUG") as cm:
                image.save()

        self.assertGreaterEqual(len(cm.output), 1)
        image.refresh_from_db()

        with open(
                "main/fixtures/Venue1.thumb.jpg",
                "rb",
        ) as f:
            expected_content = f.read()
            zimage = image.venue_thumbnail.read()
            ximage = expected_content
            self.assertEqual(zimage, ximage)

        image.venue_thumbnail.delete(save=False)
        image.image.delete(save=False)
