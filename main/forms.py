import logging

from django import forms
from django.contrib.auth import get_user_model

from phonenumber_field.formfields import PhoneNumberField

from main.models import VenueRequest, SubmitVenue

user = get_user_model()
logger = logging.getLogger(__name__)


class SendRequestForm(forms.ModelForm):
    flexible_dates = forms.BooleanField(required=False)
    message = forms.CharField(max_length=999, min_length=1, required=True, widget=forms.Textarea)
    contact_reason = forms.CharField(max_length=60, required=True,
                                     widget=forms.TextInput(attrs={'placeholder': 'مثال: جهت رزرو'}))
    preferred_wedding_date = forms.DateTimeField(required=True)
    number_of_guests = forms.IntegerField(min_value=1, max_value=10000, required=True)

    # phone_number = PhoneNumberField(validators=[validate_international_phonenumber])

    class Meta:
        model = VenueRequest
        # exclude = ('created_time', 'read',)
        fields = ('phone_number',)

class SubmitVenueForm(forms.ModelForm):
    class Meta:
        model = SubmitVenue
        fields = ('phone_number', 'user_name', 'email', 'area', 'venue_name', 'website',)

    phone_number = PhoneNumberField(label='شماره تلفن', help_text='یک شماره تلفن معتبر وارد کنید.', required=True,
                                    error_messages={
                                        'invalid': 'لطفا یک شماره تلفن معتبر وارد کنید'
                                    })
