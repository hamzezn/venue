from random import randint

from bootstrap_modal_forms.generic import BSModalLoginView, BSModalCreateView
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LogoutView
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import FormView, View

from Venues import settings
from Venues.utils import send_sms
from user import forms, models


class SingUpView(BSModalCreateView):
    template_name = 'users/sign_up.html'
    form_class = forms.SignUpForm
    success_message = 'شما به موفقیت ثبت نام شدید.'
    success_url = reverse_lazy('activate')

    def form_valid(self, form):
        request = self.request
        user = form.save(commit=False)
        if not settings.ENABLE_USER_ACTIVATION:
            user.is_active = True

        # Create a user record
        user.save()

        code = randint(100000, 999999)
        # Create an activation code
        models.Activation.objects.create(user=user, code=code)

        msg = 'کدفعال سازی شما: ' + str(code)

        send_sms(user.phone_number, msg)

        messages.success(request, 'ثبت نام انجام شد شماره تلفن وارد شده را تایید کنید.')

        response = redirect('activate')
        response.set_cookie('phone_number', user.phone_number, max_age=1000) # max age is nearly 10 minutes

        return response

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()

        if not self.request.is_ajax():
            messages.success(self.request, self.success_message)
            return redirect('activate')
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class LogInView(BSModalLoginView):
    authentication_form = forms.LogInForm
    template_name = 'users/log_in.html'
    success_message = 'شما با موفقیت وارد شدید.'
    error_message = "خطایی رخ داده دوباره تلاش کنید"
    success_url = reverse_lazy('account')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if not self.request.is_ajax() and not form.is_valid():
            messages.error(self.request, self.error_message)
            return redirect('account')
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        if not self.request.is_ajax():
            messages.error(self.request, "جهت دسترسی به صفحه مورد نظر لطفا ثبت نام کنید یا وارد شوید")
            return redirect('home')
        return self.render_to_response(self.get_context_data())


class LogOutView(LoginRequiredMixin, LogoutView):
    template_name = 'users/log_out.html'


from django.shortcuts import HttpResponse
class ActivateView(FormView):
    template_name = 'users/activate.html'
    form_class = forms.ActivationForm

    def form_valid(self, form):
        request = self.request

        phone_number = request.COOKIES.get('phone_number')

        code = form.cleaned_data['code']
        activation = models.Activation.objects.filter(code=code).first()
        user = form.user_cache

        if user.phone_number != phone_number:
            messages.error(self.request, "درخواست نادرست")
            return redirect('activate')

        user.is_active = True
        # Create a user record
        user.save()
        # Delete an activation code
        activation.delete()

        login(request, user)
        messages.success(request, 'شماره تلفن شما با موفقیت تایید شد.')
        return redirect('home')


class ResendActivationCodeView(View):
    template_name = 'users/resend_activation_code.html'

    def post(self, request, *args, **kwargs):
        phone_number = request.COOKIES.get('phone_number')

        user = models.User.objects.filter(phone_number=phone_number, is_active=False).first()

        if not user:
            messages.error(request, 'شماره تلفن وارد شده صحیح نمی باشد.')
            return redirect('activate')

        code = randint(100000, 999999)
        models.Activation.objects.create(user=user, code=code)

        msg = 'کد فعالسازی شما:  : ' + str(code)

        send_sms(user.phone_number, msg)

        messages.success(self.request, 'یک کد فعال سازی برای شماره تلفن شما ارسال شد.')

        return redirect('activate')


class RestorePasswordView(FormView):
    template_name = 'users/restore_password.html'
    form_class = forms.RestorePasswordForm

    def form_valid(self, form):
        user = form.user_cache

        code = randint(100000, 999999)
        models.Activation.objects.create(user=user, code=code)

        msg = 'کد بازیابی رمز عبور شما:‌ ' + str(code)
        send_sms(user.phone_number, msg)
        messages.success(self.request,'یک کد فعال سازی برای شماره تلفن شما ارسال شد.')

        response = redirect('restore_password_done')
        response.set_cookie('phone_number', user.phone_number, max_age=1000)  # max age is nearly 10 minutes

        return response


class RestorePasswordDoneView(FormView):
    template_name = 'users/restore_password_done.html'
    form_class = forms.RestorePasswordDoneForm

    def form_valid(self, form):
        request = self.request
        user = form.user_cache
        phone_number = request.COOKIES.get('phone_number')

        if user.phone_number != phone_number:
            messages.error(request, 'خطایی رخ داده است')
            return redirect('restore_password_done')

        code = form.cleaned_data['code']
        activation = models.Activation.objects.filter(code=code).first()
        user.is_active = True
        activation.delete()

        login(request, user)
        messages.success(request, 'اکنون میتوانید رمز عبور خودرا تغییر دهید.')
        return redirect('restore_password_confirm')


class RestorePasswordConfirmView(LoginRequiredMixin, FormView):
    template_name = 'users/restore_password_confirm.html'
    form_class = forms.RestorePasswordConfirmForm

    def form_valid(self, form):
        request = self.request
        user = request.user
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        messages.success(request, 'رمز عبور شما با موفقیت تغییر یافت')
        return redirect('home')
