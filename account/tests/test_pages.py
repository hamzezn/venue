from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse


class TestPrivatePages(TestCase):

    def setUp(self):
        """
        Create user for tests
        """
        self.client = Client()
        self.user = get_user_model().objects.create(
            name='testName',
            password='1234 qwer',
            phone_number='+989123456789',
        )

    def test_account_page(self):
        """
        Test that account page works for logged in users
        """
        self.client.login(self.user)
        response = self.client.get(reverse('account'))
        self.assertEqual(response.status_code, 200)

    def test_account_page_not_logged_in(self):
        """
        Test that account page not works for not logged in users
        """
        response = self.client.get(reverse('account'))
        self.assertEqual(response.status_code, 403)
