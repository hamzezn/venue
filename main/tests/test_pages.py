from django.test import TestCase
from django.urls import reverse


class TestPublicStaticPages(TestCase):
    def test_venue_list_page(self):
        """
        Test that venue list page works
        """
        response = self.client.get(reverse('venues_list'))
        self.assertEqual(response.status_code, 200)

    def test_home_page(self):
        """
        Test that home page works
        """
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_about_us_page(self):
        """
        Test that about us page works
        """
        response = self.client.get(reverse('about_us'))
        self.assertEqual(response.status_code, 200)
