// navbar color change on scroll
$(window).scroll(function () {
    $('#indexNavbar').toggleClass('scrolled', $(this).scrollTop() > 5);
});


// // sign Up Modal
// //set button id on click to hide first modal
// $("#signUpButton").on( "click", function() {
//   $('#signUpModal1').modal('hide');
// });
// //trigger next modal
// $("#signUpButton").on( "click", function() {
//   $('#signUpModal2').modal('show');
// });
//
// //set button id on click to hide first modal
// $("#phoneNumberEdit").on( "click", function() {
//   $('#signUpModal2').modal('hide');
// });
// //trigger next modal
// $("#phoneNumberEdit").on( "click", function() {
//   $('#signUpModal1').modal('show');
// });
//
// //set button id on click to hide first modal
// $("#otpConfirmButton").on( "click", function() {
//   $('#signUpModal2').modal('hide');
// });
// //trigger next modal
// $("#otpConfirmButton").on( "click", function() {
//   $('#signUpModal3').modal('show');
// });
//
// //set button id on click to hide first modal
// $("#signInLink").on( "click", function() {
//   $('#signUpModal1').modal('hide');
// });
// //trigger next modal
// $("#signInLink").on( "click", function() {
//   $('#logInModal').modal('show');
// });
//
// //set button id on click to hide first modal
// $("#signUpLink").on( "click", function() {
//   $('#logInModal').modal('hide');
// });
// //trigger next modal
// $("#signUpLink").on( "click", function() {
//   $('#signUpModal1').modal('show');
// });


// filter section
$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
});
$('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
});



// ceremony & reception checkboxes
$('#receptionCheckbox').click(function () {
    if ($('.receptionRadio').is(':disabled')) {
        $('.receptionRadio').removeAttr('disabled');
    } else {
        $('.receptionRadio').attr('disabled', 'disabled');
    }
});
$('#ceremonyCheckbox').click(function () {
    if ($('.ceremonynRadio').is(':disabled')) {
        $('.ceremonynRadio').removeAttr('disabled');
    } else {
        $('.ceremonynRadio').attr('disabled', 'disabled');
    }
});


// sticky sidebar filter in venues page
$(function () {
    $(".sidebar").stick_in_parent({
        offset_top: 10,
        recalc_every: 15,
    });
})

// sticky sidebar pricing in venue-details page
$(function () {
    $("#pricingStickySidebar").stick_in_parent({
        offset_top: 40,
        recalc_every: 15,
    });
})




// open selected tab via external link for contact page
function showHashTargetTab() {
    //// tab switch function. this is Bootstrap
    $('#myTab a[href="' + window.location.hash + '"]').tab('show');

    $('.package .sub-menu li').removeClass('active open');
    $('.package .sub-menu a[href*="' + window.location.hash + '"]').parent('li').addClass('active open');
}
$(document).on('click', '#myTab a[data-toggle="tab"]', function (e) {
    e.preventDefault();
    window.location.hash = $(this)[0].hash;

    //// tab switch function. this is Bootstrap again
    $(this).tab('show');
});

// this happens on load, we check if the hash exists and if it is show the specific tab, otherwise show default tab
if (window.location.hash) {
    showHashTargetTab();
} else {
    //// tab switch function. this is Bootstrap again
    $('#myTab a[data-toggle="tab"]:first').tab('show');
}
// we need to know when we change the hash to show the specific tab
$(window).on('hashchange', function (e) {
    e.preventDefault();
    showHashTargetTab();
});


// open selected tab via external link for about us page
function showHashTargetTab() {
    //// tab switch function. this is Bootstrap
    $('#sideTabs a[href="' + window.location.hash + '"]').tab('show');

    $('.package .sub-menu li').removeClass('active open');
    $('.package .sub-menu a[href*="' + window.location.hash + '"]').parent('li').addClass('active open');
}
$(document).on('click', '#sideTabs a[data-toggle="tab"]', function (e) {
    e.preventDefault();
    window.location.hash = $(this)[0].hash;

    //// tab switch function. this is Bootstrap again
    $(this).tab('show');
});

// this happens on load, we check if the hash exists and if it is show the specific tab, otherwise show default tab
if (window.location.hash) {
    showHashTargetTab();
} else {
    //// tab switch function. this is Bootstrap again
    $('#sideTabs a[data-toggle="tab"]:first').tab('show');
}
// we need to know when we change the hash to show the specific tab
$(window).on('hashchange', function (e) {
    e.preventDefault();
    showHashTargetTab();
});



// datePicker Farsi
$(document).ready(function () {
    $("#inputDate3").MdPersianDateTimePicker({
        targetDateSelector: "#inputDate3",
        targetTextSelector: "#showDate_class",
        isGregorian: false,
        textFormat: 'dddd dd MMMM yyyy h:m',
        disableBeforeToday: true,
        enableTimePicker: true,
    });
});
$('#contact').on('shown.bs.modal', function () {
    $(document).off('focusin.modal');
});


// open cantact form modal on page load
// $(window).on('load',function(){
//     $('#contact').modal('show');
// });

// open warning message on page load
$(window).on('load', function () {
    $('#warningMessage').alert('show');
});

// open success message on page load
$(window).on('load', function () {
    $('#successMessage').alert('show');
});





