from django.urls import path

from user import views

urlpatterns = [
    path('sign-up/', views.SingUpView.as_view(), name='sign_up_user'),

    path('activation-code/', views.ActivateView.as_view(), name='activate'),
    path('resend/activation-code/', views.ResendActivationCodeView.as_view(), name='resend_activation_code'),

    path('log-in/', views.LogInView.as_view(), name='log_in'),
    path('log-out/', views.LogoutView.as_view(), name='log_out'),

    path('restore/password/', views.RestorePasswordView.as_view(), name='restore_password'),
    path('restore/password/done/', views.RestorePasswordDoneView.as_view(), name='restore_password_done'),
    path('restore/password/confirm/', views.RestorePasswordConfirmView.as_view(), name='restore_password_confirm'),

    # path('change/password/', views.ChangePasswordView.as_view(), name='change_password'),
]
