import re

from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.functional import keep_lazy_text
from django.utils.html import escape
from django.utils.safestring import SafeData, mark_safe
from django.utils.text import normalize_newlines

register = Library()


@register.filter(name='persian_int')
def persian_int(english_int):
    """
    function to template engine for persian numbers
    :param english_int:
    :return persian string:
    """
    persian_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
    z = []
    english_str = str(english_int)
    for digit in english_str:
        try:
            x = persian_nums[int(digit)]
        except ValueError:
            x = digit
        z.append(x)
    return ''.join(z)


@keep_lazy_text
def our_linebreaks(value, autoescape=False):
    """Convert newlines into <p> and <br>s."""
    value = normalize_newlines(value)
    paras = re.split('\n{2,}', str(value))
    if autoescape:
        paras = ['<p class="small text-secondary">%s</p>' % escape(p).replace('\n', '<br>') for p in paras]
    else:
        paras = ['<p class="small text-secondary">%s</p>' % p.replace('\n', '<br>') for p in paras]
    return '\n\n'.join(paras)


@register.filter("our_linebreaks", is_safe=True, needs_autoescape=True)
@stringfilter
def linebreaks_filter(value, autoescape=True):
    """
    Replace line breaks in plain text with appropriate HTML; a single
    newline becomes an HTML line break (``<br>``) and a new line
    followed by a blank line becomes a paragraph break (``</p>``).
    """
    autoescape = autoescape and not isinstance(value, SafeData)
    return mark_safe(our_linebreaks(value, autoescape))
