import datetime
import random

import factory.fuzzy
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.template.defaultfilters import slugify
from factory.fuzzy import BaseFuzzyAttribute
from pytz import UTC

from main import models
from user.factories import UserFactory

User = get_user_model()


class FuzzyPoint(BaseFuzzyAttribute):
    def fuzz(self):
        return Point(random.uniform(50.50, 51.50), random.uniform(34.5, 35.5))


class ManyToManyFactory(factory.django.DjangoModelFactory):
    class Meta:
        django_get_or_create = ('slug',)

    name = factory.Sequence(lambda n: "example-%s" % n)
    slug = factory.LazyAttribute(lambda o: slugify(o.name))


class StyleManyFactory(ManyToManyFactory):
    class Meta:
        model = models.VenueStyle

    name = factory.Sequence(lambda n: "سبک-%s" % n)


class TypeManyFactory(ManyToManyFactory):
    class Meta:
        model = models.VenueType

    name = factory.Sequence(lambda n: "کاربری-%s" % n)


class PropertyManyFactory(ManyToManyFactory):
    class Meta:
        model = models.VenueProperty

    name = factory.Sequence(lambda n: "ویژگی-%s" % n)


class ServiceManyFactory(ManyToManyFactory):
    class Meta:
        model = models.VenueService

    name = factory.Sequence(lambda n: "خدمات-%s" % n)


class SpaceManyFactory(ManyToManyFactory):
    class Meta:
        model = models.VenueSpace

    name = factory.Sequence(lambda n: "فضا-%s" % n)


class AwardsManyFactory(ManyToManyFactory):
    class Meta:
        model = models.VenueAward

    name = factory.Sequence(lambda n: "افتخار-%s" % n)
    award_photo = factory.django.ImageField(color=random.choice(['blue', 'green', 'orange', 'yellow', 'red']))


class VenueProvinceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.VenueProvince
        django_get_or_create = ('slug',)

    name = factory.Sequence(lambda n: "استان-%s" % n)
    slug = factory.LazyAttribute(lambda o: o.name)


class VenueAreaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.VenueArea
        django_get_or_create = ('slug',)

    name = factory.Sequence(lambda n: "منطقه-%s" % n)
    slug = factory.LazyAttribute(lambda o: o.name)
    province = factory.Iterator(models.VenueProvince.objects.all())


class TestVenueAreaFactory(VenueAreaFactory):
    province = factory.SubFactory(VenueProvinceFactory)


class VenueFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'تالار-%d' % n)
    # phone = factory.fuzzy.FuzzyInteger(low=100000000000)
    slug = factory.LazyAttribute(lambda o: slugify(o.name))
    owner = factory.Iterator(User.objects.all())
    area = factory.Iterator(models.VenueArea.objects.all())
    min_price = factory.fuzzy.FuzzyInteger(low=10000000, step=100000)
    max_price = factory.fuzzy.FuzzyInteger(low=90000000, step=100000)
    capacity = factory.fuzzy.FuzzyInteger(low=50, step=50, high=5000)
    description = factory.fuzzy.FuzzyText(chars=['تالار ', 'توضیح ', 'عدد ', ' ۱۱۱۱', ' 2222', 'عروسی ', ' ', '\n'],
                                          length=50)
    address = factory.fuzzy.FuzzyText(chars=['کوچه ', 'پلاک ', 'خیابان ', ' ۱۲', ' 23', 'میدان ', '\n', '-'],
                                      length=30)
    rating = factory.fuzzy.FuzzyFloat(low=0, high=5, precision=2)
    website = factory.Sequence(lambda n: "www.site%d.com" % n)
    coordinates = FuzzyPoint()
    active = factory.fuzzy.FuzzyChoice([True, False, True, True])

    class Meta:
        model = models.Venue
        django_get_or_create = ('slug',)

    @factory.sequence
    def phone(n):
        a = random.randint(100000, 10000000)
        b = random.randint(1, 3)
        return '+989%d%08d' % (b, a)

    @factory.post_generation
    def styles(self, create, extracted):
        if not create:
            # # Simple build, do nothing.
            return
        if extracted:
            # A list of style were passed in, use them
            for style in extracted:
                self.styles.add(style)

    @factory.post_generation
    def spaces(self, create, extracted):
        if not create:
            # # Simple build, do nothing.
            return
        if extracted:
            # A list of style were passed in, use them
            for space in extracted:
                self.spaces.add(space)

    @factory.post_generation
    def services(self, create, extracted):
        if not create:
            # # Simple build, do nothing.
            return
        if extracted:
            # A list of style were passed in, use them
            for service in extracted:
                self.services.add(service)

    @factory.post_generation
    def types(self, create, extracted):
        if not create:
            # # Simple build, do nothing.
            return
        if extracted:
            # A list of style were passed in, use them
            for typ in extracted:
                self.types.add(typ)

    @factory.post_generation
    def properties(self, create, extracted):
        if not create:
            # # Simple build, do nothing.
            return
        if extracted:
            # A list of style were passed in, use them
            for prop in extracted:
                self.properties.add(prop)

    @factory.post_generation
    def awards(self, create, extracted):
        if not create:
            # # Simple build, do nothing.
            return
        if extracted:
            # A list of style were passed in, use them
            for award in extracted:
                self.awards.add(award)


class TestVenueFactory(VenueFactory):  # Tests have problem with  factory.Iterator
    area = factory.SubFactory(TestVenueAreaFactory)
    owner = factory.SubFactory(UserFactory)


class VenuePhotosFactory(factory.django.DjangoModelFactory):
    image = factory.django.ImageField(color=random.choice(['blue', 'green', 'orange', 'yellow', 'red']))
    venue_photo = factory.Iterator(models.Venue.objects.all())
    photographer = factory.faker.Faker('name')

    class Meta:
        model = models.VenuePhotos


class VenueDetailsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.VenueDetails
        django_get_or_create = ('container',)

    container = factory.Iterator(models.Venue.objects.all())
    # container = factory.SubFactory(VenueFactory)
    notes = factory.fuzzy.FuzzyText(chars=['تالار ', 'توضیح ', 'عدد ', ' ۱۱۱۱', ' 2222', 'عروسی ', ' ', '\n'],
                                    length=600)
    links = factory.fuzzy.FuzzyChoice(
        ['{zzz.zzz.zzz,www.wwww.www}', '{xxx.xxx.xxx,ssss.sss.sss}', '{sss.sss.sss,aaa.aaa.aaa}'])
    amenities = factory.fuzzy.FuzzyChoice(["{مرتفع, منظره زیبا}", "{زیبا, سالن کودک}", "{برج, اتاق نوزاد}"])
    restrictions = factory.fuzzy.FuzzyChoice(
        ["{خشک, محدودیت در آهنگ}", "{گلی, عدم پارکینگ}", "{کارگران وحشی, جدا جدا}"])


class VenueEstimateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.VenueEstimate
        django_get_or_create = ('container',)

    container = factory.Iterator(models.Venue.objects.all())
    base = factory.fuzzy.FuzzyInteger(low=10000, step=1000)
    Tax = factory.fuzzy.FuzzyFloat(low=0, high=15, precision=2)


class KeyValueFactory(factory.django.DjangoModelFactory):
    container = factory.Iterator(models.Venue.objects.all())
    # container = factory.SubFactory(VenueFactory)
    key = factory.fuzzy.FuzzyChoice(['نوع یک', 'نوع دو', 'نوع سه', 'نوع چهار', 'نوع پنج', 'نوع شش', 'نوع هفت', ])
    value = factory.fuzzy.FuzzyInteger(low=1000, step=1000, high=50000)


class MenuFactory(KeyValueFactory):
    class Meta:
        model = models.Menu


class BeverageFactory(KeyValueFactory):
    class Meta:
        model = models.Beverage


class OtherPriceFactory(KeyValueFactory):
    class Meta:
        model = models.OtherPrice


class DayFactory(KeyValueFactory):
    class Meta:
        model = models.Day

    value = factory.fuzzy.FuzzyFloat(low=-10, high=100, precision=2)


class MonthFactory(KeyValueFactory):
    class Meta:
        model = models.Month

    value = factory.fuzzy.FuzzyFloat(low=-10, high=100, precision=2)


class OtherCoPriceFactory(KeyValueFactory):
    class Meta:
        model = models.OtherCoPrice

    value = factory.fuzzy.FuzzyFloat(low=-10, high=100, precision=2)


class RequestFactory(factory.django.DjangoModelFactory):
    sender = factory.Iterator(User.objects.all())
    receiver = factory.Iterator(models.Venue.objects.all())
    number_of_guests = factory.fuzzy.FuzzyInteger(low=50, step=50, high=5000)
    contact_reason = factory.fuzzy.FuzzyText(chars=['تالار ', 'توضیح ', 'عدد ', 'عروسی ', ' '], length=4)
    preferred_wedding_date = factory.fuzzy.FuzzyDateTime(datetime.datetime(2020, 1, 1, tzinfo=UTC),
                                                         datetime.datetime.now(UTC) + datetime.timedelta(days=730), )
    flexible_dates = factory.fuzzy.FuzzyChoice([True, False, True])
    message = factory.fuzzy.FuzzyText(chars=['تالار ', 'توضیح ', 'عدد ', ' ۱۱۱۱', ' 2222', 'عروسی ', ' ', '\n'],
                                      length=30)
    status = factory.fuzzy.FuzzyInteger(low=0, step=1, high=3)
    read = factory.fuzzy.FuzzyChoice([True, False, True, True])

    @factory.sequence
    def phone_number(n):
        a = random.randint(100000, 10000000)
        b = random.randint(1, 3)
        return '+989%d%08d' % (b, a)

    class Meta:
        model = models.VenueRequest


def fake_fact():
    UserFactory.create_batch(5)
    StyleManyFactory.create_batch(2)
    SpaceManyFactory.create_batch(2)
    ServiceManyFactory.create_batch(2)
    TypeManyFactory.create_batch(2)
    AwardsManyFactory.create_batch(2)
    PropertyManyFactory.create_batch(2)
    VenueProvinceFactory.create_batch(1)
    VenueAreaFactory.create_batch(3)
    many_style = models.VenueStyle.random_objects.random()
    many_space = models.VenueSpace.random_objects.random()
    many_services = models.VenueService.random_objects.random()
    many_types = models.VenueType.random_objects.random()
    many_awards = models.VenueAward.random_objects.random()
    many_properties = models.VenueProperty.random_objects.random()
    VenueFactory.create_batch(5, styles=many_style, spaces=many_space, services=many_services, types=many_types,
                              properties=many_properties, awards=many_awards)
    VenueDetailsFactory.create_batch(5)
    VenueEstimateFactory.create_batch(5)
    VenuePhotosFactory.create_batch(20)
    BeverageFactory.create_batch(20)
    MonthFactory.create_batch(20)
    DayFactory.create_batch(20)
    OtherPriceFactory.create_batch(20)
    OtherCoPriceFactory.create_batch(20)
    MenuFactory.create_batch(20)
    RequestFactory.create_batch(30)
