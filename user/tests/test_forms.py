from django.test import TestCase

from user import forms
from django.contrib.auth import get_user_model, authenticate


class UserFormTest(TestCase):

    def setUp(self):
        """Creat user at the first"""
        self.phone_number = '+989123456789'
        self.password = '1234 qwer'
        self.user = get_user_model().objects.create(phone_number=self.phone_number, password=self.password,
                                                    is_active=True, is_staff=True,
                                                    is_superuser=True)
        self.user.set_password(self.password)
        self.user.save()
        self.user = authenticate(phone_number=self.phone_number, password=self.password)

    # Sign up form
    def test_sign_up_form_valid(self):
        """
         test that form valid values
        """
        forms_data = {'name': 'pejman hadavi',
                      'phone_number': '+989123456780',
                      'password1': '1234 qwer',
                      'password2': '1234 qwer',
                      }
        form = forms.SignUpForm(data=forms_data)
        self.assertTrue(form.is_valid())

    def test_sign_up_form_invalid_phone_number(self):
        """
        test that form not valid with invalid phone number
        """
        forms_data = {'name': 'pejman hadavi',
                      'phone_number': '+98912345678*',
                      'password1': '1234 qwer',
                      'password2': '1234 qwer',
                      }
        form = forms.SignUpForm(data=forms_data)
        self.assertFalse(form.is_valid())

    def test_sign_up_form_passwords_not_match(self):
        """
        test that form no valid passwords do not match
        """
        forms_data = {'name': 'pejman hadavi',
                      'phone_number': '+989123456780',
                      'password1': '1234 qwer',
                      'password2': '1234 qwert',
                      }
        form = forms.SignUpForm(data=forms_data)
        self.assertFalse(form.is_valid())

    def test_sign_up_form_used_phone_number(self):
        """ Test that sign up form is invalid with used phone number """
        forms_data = {'name': 'pejman hadavi',
                      'phone_number': self.phone_number,
                      'password1': '1234 qwer',
                      'password2': '1234 qwert',
                      }
        form = forms.SignUpForm(data=forms_data)
        self.assertFalse(form.is_valid())

    # Login Form
    def test_login_form_valid(self):
        """ Test that form is valid with valid values """
        forms_data = {'username': self.phone_number,
                      'password': self.password,
                      }
        form = forms.LogInForm(data=forms_data)
        self.assertTrue(form.is_valid())

    def test_login_form_invalid_not_registered_phone_number(self):
        """ Test that login form is invalid with not registered phone number """
        forms_data = {'username': '+989123456780',
                      'password': self.password,
                      }
        form = forms.LogInForm(data=forms_data)
        self.assertFalse(form.is_valid())

    # Change profile form
    def test_change_profile_form_valid(self):
        """ Test change profile form with valid values """
        forms_data = {
            'email': 'example@example.com',
            'name': 'Test Name',
        }
        form = forms.ChangeProfileForm(data=forms_data)
        self.assertTrue(form.is_valid())

    def test_change_profile_form_invalid_email(self):
        """ Test that change profile form will be invalid with wrong email """
        forms_data = {
            'email': 'examplecom',
            'name': 'Test Name',
        }
        form = forms.ChangeProfileForm(data=forms_data)
        self.assertFalse(form.is_valid())

    # Activation Form
    def test_activation_form_valid(self):
        """ Test that activation form will be valid with valid valud"""
        forms_data = {
            'code': 132456,
        }
        form = forms.ActivationForm(data=forms_data)
        self.assertFalse(form.is_valid())

    def test_activation_form_string_invalid(self):
        """ Test that activatoin form will be invalid with string values """
        form_data = {
            'code': 'string'
        }
        form = forms.ActivationForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_activation_form_invalid_valud(self):
        """ Test that activation form will be invalid with invalid code length bigger than 999999 """
        form_data = {
            'code': 1234567
        }
        form = forms.ActivationForm(data=form_data)
        self.assertFalse(form.is_valid())

    # **********************************************
    # We can also do this for other forms but
    # they are the same as follows and it will be overtime
    # ***********************************************