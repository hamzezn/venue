import logging

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordChangeForm

User = get_user_model()
logger = logging.getLogger(__name__)


class MultipleForm(forms.Form):
    action = forms.CharField(max_length=60, widget=forms.HiddenInput())


class ChangeAccountForm(MultipleForm):
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangeAccountForm, self).__init__(*args, **kwargs)

    email = forms.EmailField(max_length=60)
    name = forms.CharField()

    class Meta:
        model = User
        fields = ('email', 'name',)


class ChangePasswordForm(PasswordChangeForm):
    action = forms.CharField(max_length=60, widget=forms.HiddenInput())

    class Meta:
        model = User
        fields = ('old_password', 'new_password1', 'new_password2',)
