"""Venues URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from main import views
from main.views import VenueFilterView, HomePageView, AboutUsPageView, SubmitVenuePageView

urlpatterns = [
    path('wedding-venues/', VenueFilterView.as_view(), name='venues_list'),
    path('', HomePageView.as_view(), name='home'),
    path('about_us/', AboutUsPageView.as_view(), name='about_us'),
    path('location/<slug:style>', views.LocationListView.as_view(), name='venue_location'),
    path('venue/<slug:slug>/', views.VenueDetails.as_view(), name='venue_details'),
    path('venue/<slug:slug>/like', views.VenueLikeToggle.as_view(), name='venue_like_toggle'),
    path('api/venue/<slug:slug>/like', views.VenueLikeAPIToggle.as_view(), name='venue_like_api_toggle'),
    path('venue/<slug:slug>/estimate/', views.VenueEstimateView.as_view(), name='venue_estimate'),
    path('data.geojson/', views.MapDataJsonView.as_view(), name='map_json_data'),
    path('data.geojson/<slug:slug>/', views.VenueMapDataJsonView.as_view(), name='venue_map_json_data'),
    path('submit_venue/', SubmitVenuePageView.as_view(), name='submit_venue'),
]
