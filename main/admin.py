from django.contrib import admin
from django.utils.html import format_html
from leaflet.admin import LeafletGeoAdmin

from django.core.exceptions import ValidationError

# Register your models here.
from dj_array_field_persian.admin.mixins import DynamicArrayMixin
from main.models import Venue, VenueAward, VenueStyle, VenueDetails, VenueEstimate, VenuePhotos, \
    VenueProperty, VenueService, VenueSpace, VenueType, VenueRequest, VenueArea, VenueProvince, Menu, Day, Beverage, \
    Month, OtherPrice, OtherCoPrice, SubmitVenue, OtherFields


def make_active(queryset):
    queryset.update(active=True)


make_active.short_description = "فعال سازی انتخاب شده ها"


def make_inactive(queryset):
    queryset.update(active=False)


make_inactive.short_description = "غیر فعال سازی انتخاب شده ها"


@admin.register(VenueAward)
class VenueAwardAdmin(admin.ModelAdmin):
    exclude = ('awards_thumbnail',)
    list_display = (
        "name",
    )
    readonly_fields = ("awards_thumbnail_tag",)
    search_fields = ("name",)
    prepopulated_fields = {"slug": ("name",)}

    @staticmethod
    def awards_thumbnail_tag(obj):
        if obj.awardsthumbnail:
            return format_html('<img src="%s"/>' % obj.awardsthumbnail.url)
        return "-"

    # this defines the column name for the list_display
    awards_thumbnail_tag.short_description = "پیش نمایش"


# @admin.register(VenueDetails)
# class VenueDetailsAdmin(admin.ModelAdmin, DynamicArrayMixin):
#     pass


class VenueDetailsInline(admin.StackedInline, DynamicArrayMixin):
    model = VenueDetails
    extra = 1


@admin.register(VenueEstimate)
class VenueEstimateAdmin(admin.ModelAdmin):
    pass


class VenueEstimateInline(admin.StackedInline):
    model = VenueEstimate
    extra = 0


@admin.register(VenueRequest)
class VenueRequestAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'receiver', 'contact_reason', 'read']
    exclude = ('sender', 'read', 'status')


    def get_object(self, request, object_id, from_field=None):
        queryset = self.get_queryset(request)
        model = queryset.model
        field = model._meta.pk if from_field is None else model._meta.get_field(from_field)
        try:
            object_id = field.to_python(object_id)
            object = VenueRequest.objects.get(id=object_id)
            object.read = True
            object.save()
            return queryset.get(**{field.name: object_id})
        except (model.DoesNotExist, ValidationError, ValueError):
            return None

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return True

    def has_change_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(VenueRequestAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(receiver__owner=request.user)


@admin.register(VenuePhotos)
class VenuePhotosAdmin(admin.ModelAdmin):
    exclude = ("venue_thumbnail",)
    list_display = ['venue_photo', 'image', "venue_thumbnail_tag", 'active']
    list_editable = ['active']
    readonly_fields = ("venue_thumbnail_tag",)
    search_fields = ("venue_photo__name",)

    def get_queryset(self, request):
        return VenuePhotos.objects.all()

    def venue_thumbnail_tag(self, obj):
        if obj.venue_thumbnail:
            return format_html('<img src="%s"/>' % obj.venue_thumbnail.url)
        return "-"

    # this defines the column name for the list_display
    venue_thumbnail_tag.short_description = "پیش نمایش"


class VenuePhotosInline(admin.TabularInline):
    model = VenuePhotos
    exclude = ("venue_thumbnail",)
    readonly_fields = ("venue_thumbnail_tag",)
    extra = 0

    def venue_thumbnail_tag(self, obj):
        if obj.venue_thumbnail:
            return format_html('<img src="%s"/>' % obj.venue_thumbnail.url)
        return "-"

    # this defines the column name for the list_display
    venue_thumbnail_tag.short_description = "پیش نمایش"


@admin.register(VenueProperty)
class VenuePropertyAdmin(admin.ModelAdmin):
    exclude = ("id",)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)


@admin.register(VenueStyle)
class VenueStyleAdmin(admin.ModelAdmin):
    exclude = ("id",)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)


@admin.register(VenueService)
class VenueServiceAdmin(admin.ModelAdmin):
    exclude = ("id",)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)


@admin.register(VenueSpace)
class VenueSpaceAdmin(admin.ModelAdmin):
    exclude = ("id",)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)


@admin.register(VenueType)
class VenueTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)


@admin.register(VenueProvince)
class VenueProvinceAdmin(admin.ModelAdmin):
    exclude = ("id",)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)


@admin.register(VenueArea)
class VenueAreaAdmin(admin.ModelAdmin):
    exclude = ("id",)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ("name",)
    # autocomplete_fields = ("province",)


class MenuInline(admin.TabularInline):
    model = Menu
    extra = 0


class DayInline(admin.TabularInline):
    model = Day
    extra = 0


class BeverageInline(admin.TabularInline):
    model = Beverage
    extra = 0


class MonthInline(admin.TabularInline):
    model = Month
    extra = 0


class OtherPriceInline(admin.TabularInline):
    model = OtherPrice
    extra = 0


class OtherCoPriceInline(admin.TabularInline):
    model = OtherCoPrice
    extra = 0


@admin.register(Venue)
class VenuesAdmin(LeafletGeoAdmin):
    settings_overrides = {
        'DEFAULT_CENTER': (35.696341, 51.405904),
        'DEFAULT_ZOOM': 11,
        # 'MIN_ZOOM': 3,
        # 'MAX_ZOOM': 18,
    }
    exclude = ("id",)
    fieldsets = (
        ('مشخصات تالار', {
            'classes': ('expand', 'extrapretty', 'wide'),
            "fields": (("name", "phone"), "slug")
        }),
        ('موقعیت مکانی', {
            'classes': ('expand', 'extrapretty', 'wide'),
            "fields": ("area", "address", "coordinates")}),
        ('درباره تالار', {
            'classes': ('expand', 'extrapretty',),
            "fields": (
                ("types", "min_price", 'max_price'),
                ("styles", "services", "spaces", "properties", "awards", "capacity"),

                "description", "owner")}),
        ('سایر', {
            'classes': ('collapse', 'extrapretty', 'wide'),
            "fields": ("website", "rating", "active", "updated_time", "created_time")}),
    )
    list_display = ("name", "min_price", "max_price", "capacity", "active",)
    list_editable = ("min_price", "max_price", "capacity", "active",)
    readonly_fields = ("updated_time", "created_time")
    list_filter = ("active", "types", "styles", "rating", "updated_time")
    search_fields = ("name", "owner__email")
    prepopulated_fields = {"slug": ("name", "phone")}
    autocomplete_fields = ("types", "spaces", "styles", "services", "properties", "awards", "area")
    inlines = [VenuePhotosInline, VenueDetailsInline, MenuInline, BeverageInline, DayInline, MonthInline,
               OtherPriceInline, OtherCoPriceInline, VenueEstimateInline]
    actions = [make_active, make_inactive]

    def get_queryset(self, request):
        qs = super(VenuesAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return self.readonly_fields
        return list(self.readonly_fields) + ["slug", "active", "owner", "rating", ]

    def get_prepopulated_fields(self, request, obj=None):
        if request.user.is_superuser:
            return self.prepopulated_fields
        else:
            return {}

    def get_list_display(self, request):
        """
        Return a sequence containing the fields to be displayed on the
        changelist.
        """
        if request.user.is_superuser:
            return list(self.list_display) + ["owner"]
        else:
            return self.list_display

    def save_model(self, request, obj, form, change):
        """
        if owner=blank set requested user as owner
        """
        if getattr(obj, 'owner', None) is None:
            obj.owner = request.user
        obj.save()


@admin.register(SubmitVenue)
class SubmitVenue(admin.ModelAdmin):
    list_display = ('user_name', 'venue_name', 'area', 'phone_number')
    exclude = ('id',)
    readonly_fields = ['phone_number', 'user_name', 'email', 'website', 'venue_name', 'area']


@admin.register(OtherFields)
class Other(admin.ModelAdmin):
    pass


class VenueOwnerAdmin(VenuesAdmin):
    list_editable = ("min_price", "max_price",)
    # readonly_fields = ("name", "slug", "active", "owner", "rating", "awards", "updated_time")
    prepopulated_fields = {}


# The class below will pass to the Django Admin templates a couple
# of extra values that represent colors of headings
class OtherAdminSite(admin.sites.AdminSite):
    pass


# The following will add reporting views to the list of
# available urls and will list them from the index page
class OwnerAdminSite(OtherAdminSite):
    site_header = "پنل ثبت تالار"

    def has_permission(self, request):
        return request.user.is_active and request.user.is_venue_owner


admin.site.site_header = 'سایت'
owner_admin = OwnerAdminSite("owner_admin")
owner_admin.register(Venue, VenueOwnerAdmin)
owner_admin.register(VenueArea, VenueAreaAdmin)
owner_admin.register(VenueStyle, VenueStyleAdmin)
owner_admin.register(VenueType, VenueTypeAdmin)
owner_admin.register(VenueService, VenueServiceAdmin)
owner_admin.register(VenueSpace, VenueSpaceAdmin)
owner_admin.register(VenueAward, VenueAwardAdmin)
owner_admin.register(VenueProperty, VenuePropertyAdmin)
owner_admin.register(VenueRequest, VenueRequestAdmin)
