from django.db.models import Q
from django.test import TestCase, override_settings
from django.urls import reverse, resolve
from django.utils import html

from main import models
from main.factories import TestVenueFactory
from main.views import HomePageView, AboutUsPageView


@override_settings(ROOT_URLCONF='Venues.urls')
class GenericViewTestCase(TestCase):
    def setUp(self):
        TestVenueFactory.create_batch(3, active=True)
        TestVenueFactory.create_batch(4, active=False)
        ab = TestVenueFactory(name="Flowers", capacity=1000, min_price=10000, active=True)
        b = ab.styles.create(name="سلطنتی", slug="Sltnt")
        ac = TestVenueFactory(name="Things", active=True)
        c = ac.styles.create(name="فانتزی", slug="Fntzi")
        ad = TestVenueFactory(name="Nights", active=True)
        d = ad.styles.create(name="زیبی", slug="zibiii")
        TestVenueFactory(name="Memories", active=False)
        TestVenueFactory(name="Disabled", active=False)


class TestPage(GenericViewTestCase):
    base_filter_url = '/wedding-venues/'
    paginate_by = 10

    def test_homepage_page_works(self):
        view = resolve('/')
        self.assertEqual(view.func.__name__, HomePageView.as_view().__name__)
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/index.html")
        self.assertContains(response, 'تالار')

    def test_about_us_page_works(self):
        view = resolve('/about_us/')
        self.assertEqual(view.func.__name__, AboutUsPageView.as_view().__name__)
        response = self.client.get(reverse("about_us"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/about_us.html")
        self.assertContains(response, 'تالار')

    def test_venue_page_filter_returns_active(self):
        response = self.client.get(self.base_filter_url)
        self.assertEqual(response.status_code, 200)
        for i in ['Flowers', 'Things', 'Nights']:
            self.assertContains(response, html.escape(i))
        for j in ['Memories', 'Disabled']:
            self.assertNotContains(response, html.escape(j))
        venue_list = models.Venue.active_objects.all().order_by("rating", "-updated_time")[:self.paginate_by]
        self.assertEqual(list(response.context['object_list']), list(venue_list))
        self.assertNotEqual(list(response.context['object_list']), [])

    def test_venue_page_filters_by_style_and_active(self):
        style_id = models.VenueStyle.objects.filter(slug="Sltnt").values('pk').first()['pk']
        response = self.client.get(self.base_filter_url + '?styles=%s' % style_id)
        self.assertEqual(response.status_code, 200)
        venue_list = models.Venue.active_objects.filter(styles__slug="Sltnt").order_by("rating", "-updated_time")[
                     :self.paginate_by]
        self.assertEqual(list(response.context['object_list']), list(venue_list))
        self.assertNotEqual(list(response.context['object_list']), [])

    def test_venue_page_filters_by_search_name(self):
        """ search filter is area,name,style filter"""
        name = models.Venue.objects.all().values('name').first()['name']
        response = self.client.get(self.base_filter_url + '?search=%s' % name)
        self.assertEqual(response.status_code, 200)
        venue_list = models.Venue.active_objects.filter(
            Q(area__name__contains=name) | Q(name__contains=name)).order_by("rating", "-updated_time")[
                     :self.paginate_by]
        self.assertEqual(list(response.context['object_list']), list(venue_list))
        response = self.client.get(self.base_filter_url + '?search=%s' % "")
        self.assertEqual(response.status_code, 200)
        venue_list = models.Venue.active_objects.all().order_by("rating", "-updated_time")[:self.paginate_by]
        self.assertEqual(list(response.context['object_list']), list(venue_list))
        self.assertNotEqual(list(response.context['object_list']), [])

    def test_venue_page_filters_by_search_area(self):
        """ search filter is area,name,style filter"""
        # area_name = models.VenueArea.objects.order_by('?').values('name').first()['name']
        area_name = 'ا'
        response = self.client.get(self.base_filter_url + '?search=%s' % area_name)
        self.assertEqual(response.status_code, 200)
        venue_list = models.Venue.active_objects.filter(
            Q(area__name__contains=area_name) | Q(name__contains=area_name)).order_by("rating", "-updated_time")[
                     :self.paginate_by]
        self.assertEqual(list(response.context['object_list']), list(venue_list))
        self.assertTrue(len(response.context['object_list']) > 0)
        self.assertNotEqual(list(response.context['object_list']), [])
