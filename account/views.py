from django.contrib import messages
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.
from django.shortcuts import redirect
from django.urls import reverse_lazy

from account.forms import ChangeAccountForm, ChangePasswordForm
from account.multiform import MultiFormsView, ProcessMultipleFormsView
from main import models

User = get_user_model()


class AccountView(LoginRequiredMixin, MultiFormsView, ProcessMultipleFormsView):
    template_name = "account.html"
    form_classes = {
        'change_account': ChangeAccountForm,
        'change_password': ChangePasswordForm,
    }

    success_urls = {
        'change_password': reverse_lazy('home'), }

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super(AccountView, self).get_context_data(**kwargs)
        context['favorite_venues'] = user.favorite_venues.all()
        context['Venue_requests'] = models.VenueRequest.objects.filter(sender=user).order_by("-created_time")
        return context

    def get_change_account_initial(self):
        initial = {}
        user = self.request.user
        initial['email'] = user.email
        initial['name'] = user.name
        return initial

    def change_account_form_valid(self, form):
        user = self.request.user
        user.name = form.cleaned_data['name']
        user.email = form.cleaned_data['email']
        user.save()
        messages.success(self.request, 'مشخصات کاربری با موفقیت به روز شد')
        # return self.render_to_response(self.get_context_data(forms={'change_account': form,
        #                                                             'change_password': ChangePasswordForm}))
        return redirect('account')

    def change_password_form_valid(self, form):
        form.save()
        # Updating the password logs out all other sessions for the user
        # except the current one.
        update_session_auth_hash(self.request, form.user)
        messages.success(self.request, 'گذرواژه با موفقیت به روز شد', )
        return redirect('account')

    def change_account_form_invalid(self, form):
        """If the form is invalid, render the invalid form."""
        messages.warning(self.request, 'مقدار ورودی نامعتبر است')
        return self.render_to_response(self.get_context_data(forms={'change_account': form,
                                                                    'change_password': ChangePasswordForm}))

    def change_password_form_invalid(self, form):
        """If the form is invalid, render the invalid form."""
        messages.warning(self.request, 'مقدار ورودی نامعتبر است')
        return self.render_to_response(self.get_context_data(forms={'change_password': form,
                                                                    'change_account': ChangeAccountForm}))
