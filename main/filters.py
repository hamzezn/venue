import itertools

import django_filters
from django.db.models import Q

from main import models
from main.models import VenueStyle, VenueService, VenueArea


class VenueFilter(django_filters.FilterSet):
    styles = django_filters.ModelMultipleChoiceFilter(queryset=VenueStyle.active_objects.all(), )
    services = django_filters.ModelMultipleChoiceFilter(queryset=VenueService.active_objects.all(), )
    capacity = django_filters.RangeFilter()
    min_price = django_filters.RangeFilter()
    area = django_filters.ModelMultipleChoiceFilter(queryset=VenueArea.active_objects.all(), )
    search = django_filters.CharFilter(label="Search", method='filter_search')
    search_fields = ['name']
    search_m2m_fields = ['area']

    class Meta:
        model = models.Venue
        fields = ['styles', 'capacity', 'min_price', 'services', 'area', 'search']

    def filter_search(self, qs, name, value):
        if value:
            q_parts = value.split()
            list_m2m = self.search_m2m_fields
            list0 = self.search_fields
            list1 = list_m2m + list0
            list2 = q_parts
            perms = [zip(x, list2) for x in itertools.permutations(list1, len(list2))]

            # Use a global q_totals
            q_totals = Q()
            for perm in perms:
                q_part = Q()
                for p in perm:
                    if p[0] in list_m2m:
                        q_part = q_part & Q(**{p[0] + '__name__icontains': p[1]})
                    else:
                        q_part = q_part & Q(**{p[0] + '__icontains': p[1]})
                q_totals = q_totals | q_part
            qs = qs.filter(q_totals)
        return qs
